import asyncio
import platform
import re
import ssl

import pytest

from aiomas.exceptions import RemoteException, SerializationError
import aiomas.rpc


ON_WINDOWS = platform.system() == 'Windows'


class Context:
    def __init__(self, loop, port):
        self.loop = loop
        self.addr = ('127.0.0.1', port)
        self.server = None
        self.server_connections = set()

    async def connect(self, **kwargs):
        return await aiomas.rpc.open_connection(
            self.addr, loop=self.loop, **kwargs)

    async def start_server(self, service, **kwargs):
        extra_cb = None
        if 'client_connected_cb' in kwargs:
            extra_cb = kwargs.pop('client_connected_cb')

        def client_connected_cb(rpc_client):
            self.server_connections.add(rpc_client)
            if extra_cb is not None:
                extra_cb(rpc_client)

        self.server = await aiomas.rpc.start_server(
            self.addr, service, client_connected_cb,
            loop=self.loop, **kwargs)

    async def start_server_and_connect(self, server_service,
                                       client_service=None, server_kwargs=None,
                                       client_kwargs=None):
        if server_kwargs is None:
            server_kwargs = {}

        if client_kwargs is None:
            client_kwargs = {}
        await self.start_server(server_service, **server_kwargs)
        return await self.connect(rpc_service=client_service, **client_kwargs)

    async def close_server(self):
        if self.server is not None:
            server, self.server = self.server, None
            await asyncio.gather(
                *(c.close() for c in self.server_connections), loop=self.loop)
            server.close()
            await server.wait_closed()


@pytest.fixture
def ctx(event_loop, unused_tcp_port):
    ctx = Context(event_loop, unused_tcp_port)
    yield ctx

    # Shutdown the server and wait for all pending tasks to finish:
    aiomas.run(ctx.close_server(), loop=event_loop)
    tasks = [t for t in asyncio.Task.all_tasks(event_loop) if not t.done]
    aiomas.run(asyncio.gather(*tasks, loop=event_loop), loop=event_loop)


#
# RPC handlers
#


class Service:
    router = aiomas.rpc.Service(['sub_handler'])

    def __init__(self):
        self.sub_handler = SubService()

    @router.expose
    def do_stuff(self, a, b, c):
        return (a, b, c)


class SubService:
    router = aiomas.rpc.Service(['sub_sub_handler'])

    class SubSubService:
        router = aiomas.rpc.Service()

    def __init__(self):
        self.sub_sub_handler = aiomas.rpc.ServiceDict()

    @router.expose
    def reverse_stuff(self, a, b, c):
        return (c, b, a)


@aiomas.rpc.expose
def do_stuff(a, b, c):
    return (a, b, c)


@aiomas.rpc.expose
def reverse_stuff(a, b, c):
    return (c, b, a)


dict_router = aiomas.rpc.ServiceDict({
    'do_stuff': do_stuff,
    'sub_handlers': aiomas.rpc.ServiceDict({
        'reverse_stuff': reverse_stuff,
    }),
    'sub_cls': SubService(),
})


#
# Actual tests
#


def test_router_name_path(event_loop):
    """Test that the attributes and properties "name", "parent" and "path"
    are correct for router hierarchies."""
    h = Service()
    assert h.router.name == ''
    assert h.router.parent is None
    assert h.router.path == ''

    assert h.sub_handler.router.name == 'sub_handler'
    assert h.sub_handler.router.parent == h.router
    assert h.sub_handler.router.path == 'sub_handler'

    assert h.sub_handler.sub_sub_handler.router.name == 'sub_sub_handler'
    assert h.sub_handler.sub_sub_handler.router.parent == h.sub_handler.router
    assert h.sub_handler.sub_sub_handler.router.path == \
        'sub_handler/sub_sub_handler'


@pytest.mark.asyncio
async def test_simple_rpc_with_class(ctx):
    rpc_con = await ctx.start_server_and_connect(Service())
    inputs = [1, 3.14, 'spam']
    result = await rpc_con.remote.do_stuff(*inputs)
    assert result == inputs
    await rpc_con.close()


@pytest.mark.asyncio
async def test_simple_rpc_with_dict(ctx):
    rpc_con = await ctx.start_server_and_connect(dict_router)
    inputs = [1, 3.14, 'spam']
    result = await rpc_con.remote.do_stuff(*inputs[:2], c=inputs[2])
    assert result == inputs
    await rpc_con.close()


@pytest.mark.parametrize(['handler', 'subhandler'], [
    (Service(), 'sub_handler'),
    (dict_router, 'sub_handlers'),
    (dict_router, 'sub_cls'),
])
@pytest.mark.asyncio
async def test_nested_calls(ctx, handler, subhandler):
    rpc_con = await ctx.start_server_and_connect(handler)
    inputs = [1, 3.14, 'spam']
    sh = getattr(rpc_con.remote, subhandler)
    result = await sh.reverse_stuff(*inputs)
    assert result == list(reversed(inputs))
    await rpc_con.close()


@pytest.mark.asyncio
async def test_coroutine_rpc(ctx):
    """Test an RPC which is not just a plain function but a coroutine."""
    @aiomas.rpc.expose
    async def func(*, factor):
        await asyncio.sleep(0.0001, loop=ctx.loop)
        return factor

    handlers = aiomas.rpc.ServiceDict({'func': func})
    rpc_con = await ctx.start_server_and_connect(handlers)
    result = await rpc_con.remote.func(factor=2)
    assert result == 2
    await rpc_con.close()


@pytest.mark.asyncio
async def test_coroutine_rpc_with_errors(ctx):
    """Test if errors raised in RPCs are forwarded correctly."""
    @aiomas.rpc.expose
    async def func(*, factor):
        await asyncio.sleep(0.0001, loop=ctx.loop)
        raise ValueError(42)

    handlers = aiomas.rpc.ServiceDict({'func': func})
    rpc_con = await ctx.start_server_and_connect(handlers)
    with pytest.raises(RemoteException) as exc_info:
        await rpc_con.remote.func(factor=2)

    assert 'ValueError: 42' in str(exc_info.value)
    await rpc_con.close()


@pytest.mark.asyncio
async def test_lookup_error_name_not_found(ctx):
    """Test if an error is raised if we call a non-existing function."""
    rpc_con = await ctx.start_server_and_connect(dict_router)
    with pytest.raises(RemoteException) as exc_info:
        await rpc_con.remote.func()

    assert 'LookupError: Name "func" not found' in str(exc_info.value)
    await rpc_con.close()


@pytest.mark.asyncio
async def test_lookup_error_not_exposed(ctx):
    """Test if an error is raised if we call a non-exposed function."""
    rpc_con = await ctx.start_server_and_connect(
        aiomas.rpc.ServiceDict({'func': lambda: 3}))
    with pytest.raises(RemoteException) as exc_info:
        await rpc_con.remote.func()

    assert ('LookupError: "aiomas.rpc.ServiceDict.func" is not exposed'
            in str(exc_info.value))
    await rpc_con.close()


@pytest.mark.asyncio
async def test_call_proxy(ctx):
    """An error should be raised if we directly call the proxy object."""
    rpc_con = await ctx.start_server_and_connect(dict_router)
    with pytest.raises(AttributeError) as exc_info:
        await rpc_con.remote()  # oops

    assert 'No RPC function name specified' in str(exc_info.value)
    await rpc_con.close()


def test_service_descriptor():
    class Spam:
        router = aiomas.rpc.Service()

    assert type(Spam.router) is aiomas.rpc.Service
    assert type(Spam().router) is aiomas.rpc.Router


@pytest.mark.asyncio
async def test_bidirectional_rpc(ctx):
    """Test if the server can make RPC to the client if the client passes
    its service instance to the server."""
    class ServerService:
        router = aiomas.rpc.Service()

        @router.expose
        async def server_func(self, client_proxy, value):
            res = await client_proxy.client_func('eggs')
            assert res == 'eggs'
            return value

    class ClientService:
        router = aiomas.rpc.Service()

        @router.expose
        def client_func(self, value):
            return value

    client_service = ClientService()
    rpc_con = await ctx.start_server_and_connect(ServerService(),
                                                 client_service)
    res = await rpc_con.remote.server_func(client_service, 'spam')
    assert res == 'spam'

    await rpc_con.close()


@pytest.mark.asyncio
async def test_invalid_rpc_server_router(ctx):
    with pytest.raises(ValueError) as exc_info:
        await ctx.start_server(object())

    assert 'is not a valid RPC service' in str(exc_info.value)


@pytest.mark.asyncio
async def test_invalid_rpc_client_router(ctx):
    await ctx.start_server(dict_router)
    with pytest.raises(ValueError):
        await ctx.connect(rpc_service=object())


def test_expose_nonfunc():
    pytest.raises(ValueError, aiomas.rpc.expose, object())


def test_set_invalid_sub_router():
    r1 = aiomas.rpc.ServiceDict({}).router
    r2 = aiomas.rpc.ServiceDict({}).router
    r3 = aiomas.rpc.ServiceDict({}).router

    excinfo = pytest.raises(ValueError, r1.set_sub_router, None, 'spam')
    assert 'is not a valid router' in str(excinfo.value)

    r1.set_sub_router(r3, 'spam')
    excinfo = pytest.raises(ValueError, r2.set_sub_router, r3, 'spam')
    assert 'is already a sub service' in str(excinfo.value)


def test_rpc_servide_attr_is_readonly():
    class Spam:
        router = aiomas.rpc.Service()

    with pytest.raises(AttributeError):
        Spam().router = 'foo'


def test_base_not_implemented():
    codec = aiomas.codecs.Codec()
    pytest.raises(NotImplementedError, codec.encode, 'spam')
    pytest.raises(NotImplementedError, codec.decode, 'spam')


@pytest.mark.asyncio
async def test_proxy_str_and_repr(ctx):
    rpc_con = await ctx.start_server_and_connect(Service())
    assert str(rpc_con.remote.do_stuff) == "Proxy({!r}, 'do_stuff')".format(
        ctx.addr)
    assert re.match(r"<aiomas.rpc.Proxy\({}, 'do_stuff'\) at "
                    r"0x[0-9a-f]+>".format(re.escape(repr(ctx.addr))),
                    repr(rpc_con.remote.do_stuff))
    await rpc_con.close()


def test_proxy_eq_and_hash():
    ch_a = aiomas.channel.Channel(None, aiomas.codecs.JSON, None, None)
    ch_b = aiomas.channel.Channel(None, aiomas.codecs.JSON, None, None)
    p_a = aiomas.rpc.Proxy(ch_a, 'spam')
    p_b = aiomas.rpc.Proxy(ch_a, 'spam')
    p_c = aiomas.rpc.Proxy(ch_a, 'eggs')
    p_d = aiomas.rpc.Proxy(ch_b, 'spam')

    assert p_a == p_b
    assert p_a != p_c
    assert p_a != p_d

    assert hash(p_a) == hash(p_b)
    assert hash(p_a) != hash(p_c)
    assert hash(p_a) != hash(p_d)

    d = {p_a: 1}
    d[p_b] = 2
    assert d == {p_a: 2}


@pytest.mark.asyncio
async def test_rpc_service_serialization_error_1(ctx):
    """Test that random objects can only be serialized if they are actually an
    RPC Service."""
    class ServerService:
        router = aiomas.rpc.Service()

        @router.expose
        def server_func(self, client_proxy):
            return

    class ClientService:
        router = aiomas.rpc.Service()

    rpc_con = await ctx.start_server_and_connect(ServerService(),
                                                 ClientService())
    with pytest.raises(SerializationError) as exc_info:
        await rpc_con.remote.server_func(object())
    assert 'No serializer found for type' in str(exc_info.value)

    await rpc_con.close()


@pytest.mark.asyncio
async def test_rpc_service_serialization_error_2(ctx):
    """Test that RPC Service instances can only be serialized, if a service
    is running."""
    class ServerService:
        router = aiomas.rpc.Service()

        @router.expose
        async def server_func(self, client_proxy):
            return

    class ClientService:
        router = aiomas.rpc.Service()

    # Only start a server service, but no client service:
    rpc_con = await ctx.start_server_and_connect(ServerService())
    with pytest.raises(SerializationError) as exc_info:
        await rpc_con.remote.server_func(ClientService())
    assert 'No RPC service running' in str(exc_info.value)

    await rpc_con.close()


@pytest.mark.asyncio
async def test_rpc_service_serialization_error_3(ctx):
    """Test that an RPC service can only be serialized if it belongs to the
    connections router (or one of its children)."""
    class ServerService:
        router = aiomas.rpc.Service()

        @router.expose
        async def server_func(self, client_proxy):
            return

    class ClientService:
        router = aiomas.rpc.Service()

    rpc_con = await ctx.start_server_and_connect(ServerService(),
                                                 ClientService())
    with pytest.raises(SerializationError) as exc_info:
        await rpc_con.remote.server_func(ClientService())
    assert 'is not exposed for this connection' in str(exc_info.value)

    await rpc_con.close()


@pytest.mark.asyncio
async def test_rpc_serialization_error_in_args(ctx):
    @aiomas.expose
    def make_error(arg):
        return 1

    router = aiomas.rpc.ServiceDict({
        'make_error': make_error,
    })
    rpc_con = await ctx.start_server_and_connect(router)
    with pytest.raises(SerializationError):
        await rpc_con.remote.make_error(2j)
    await rpc_con.close()


@pytest.mark.asyncio
async def test_rpc_serialization_error_in_return_value(ctx):
    @aiomas.expose
    def make_error():
        return 1j

    router = aiomas.rpc.ServiceDict({
        'make_error': make_error,
    })
    rpc_con = await ctx.start_server_and_connect(router)
    with pytest.raises(RemoteException):
        await rpc_con.remote.make_error()
    await rpc_con.close()


@pytest.mark.asyncio
async def test_ssl(ctx, certs):
    server_kwargs = {'ssl': aiomas.util.make_ssl_server_context(*certs)}
    client_kwargs = {'ssl': aiomas.util.make_ssl_client_context(*certs)}

    rpc_con = await ctx.start_server_and_connect(
        dict_router, server_kwargs=server_kwargs, client_kwargs=client_kwargs)

    inputs = [1, 3.14, 'spam']
    result = await rpc_con.remote.do_stuff(*inputs[:2], c=inputs[2])
    assert result == inputs
    await rpc_con.close()


@pytest.mark.asyncio
async def test_ssl_error(ctx, certs):
    server_ctx = aiomas.util.make_ssl_server_context(*certs)
    client_ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_1)
    client_ctx.set_ciphers('ECDH+AESGCM')
    client_ctx.verify_mode = ssl.CERT_REQUIRED
    client_ctx.load_verify_locations(cafile=certs[0])
    client_ctx.check_hostname = True
    client_ctx.load_cert_chain(certfile=certs[1], keyfile=certs[2])

    await ctx.start_server(dict_router, ssl=server_ctx)

    with pytest.raises(ssl.SSLError):
        await ctx.connect(ssl=client_ctx)


@pytest.mark.parametrize('timeout', [1, None])
@pytest.mark.asyncio
async def test_connect_timeout(ctx, timeout):
    """Test that we don't get a timeout error if we start the server only a
    little late.

    Notes:

    - With a timeout "0", we always get an error if we start the server after
      the client – even if we don't sleep.

    - If we delay the server for only .5 seconds, a timeout of 1 and "None"
      (wait indefinitely) shoud suffice.

    """
    t_connect = ctx.loop.create_task(ctx.connect(timeout=timeout))
    await asyncio.sleep(.5, loop=ctx.loop)
    await ctx.start_server(Service())
    rpc_con = await t_connect
    await rpc_con.close()


@pytest.mark.parametrize('timeout', [0, 1])
@pytest.mark.asyncio
async def test_connect_timeout_error(ctx, timeout):
    """We should get an error if the delay between client and server start is
    to big."""
    t_connect = ctx.loop.create_task(ctx.connect(timeout=timeout))
    sleep = (timeout * 2 + 2) if ON_WINDOWS else (timeout * 2)
    await asyncio.sleep(sleep, loop=ctx.loop)
    await ctx.start_server(Service())
    with pytest.raises((ConnectionRefusedError, FileNotFoundError)):
        await t_connect


@pytest.mark.asyncio
async def test_on_connection_reset_callback(ctx):
    """Test if the "on_connection_reset" callback gets called if the client
    side registers one and the server closes its connection."""
    cb_called = ctx.loop.create_future()

    def on_connection_reset_callback(exc):
        assert isinstance(exc, ConnectionError)
        cb_called.set_result(True)

    def client_connected_callback(rpc_client):
        rpc_client.on_connection_reset(on_connection_reset_callback)

    await ctx.start_server(Service(),
                           client_connected_cb=client_connected_callback)

    rpc_con = await ctx.connect()
    await rpc_con.close()

    assert (await cb_called)


@pytest.mark.parametrize('exc, cb, with_service', [
    (RuntimeError, None, False),  # No service started
    (ValueError, 23, True),  # Invalid callback type
])
def test_on_connection_reset_callback_error(ctx, exc, cb, with_service):
    """Test exceptions thrown by "on_connection_reset()" with wrong arguments.
    """
    service = Service() if with_service else None
    rpc_client = aiomas.rpc.RpcClient(
        aiomas.channel.Channel(None, aiomas.JSON(), None, ctx.loop),
        rpc_service=service)

    pytest.raises(exc, rpc_client.on_connection_reset, cb)
    if rpc_client.service is not None:
        rpc_client.service.cancel()
        try:
            aiomas.run(rpc_client.service, loop=ctx.loop)
        except asyncio.CancelledError:
            pass


@pytest.mark.asyncio
async def test_close_connection_during_rpc(ctx):
    """If an RPC closes the connection to a client during an RPC, the server
    can no longer send the result of that call to the client.  But no error
    should be raised in that case.

    .. warning::

       The test cannot fail directly (by raising an error).  Instead, failure
       is indicated by the following output of ``py.test -vs``::

          tests/test_rpc.py::test_close_connection_during_rpc
            Task exception was never retrieved future:
              <Task finished coro=<_handle_request() done, defined at ...>
                exception=ConnectionResetError('Connection closed',)>
          Traceback (most recent call last):
              ...
          ConnectionResetError: Connection closed

       I currently don't know how to retrieve that exception because the task
       is registered nowhere.

    """
    class Server:
        router = aiomas.rpc.Service()

        def __init__(self):
            self.clients = []

        def client_connected(self, client):
            self.clients.append(client)

        @aiomas.expose
        async def close_connection(self):
            while self.clients:
                client = self.clients.pop()
                await client.close()

    server = Server()
    await ctx.start_server(server,
                           client_connected_cb=server.client_connected)
    rpc_con = await ctx.connect()

    with pytest.raises(ConnectionResetError):
        await rpc_con.remote.close_connection()
