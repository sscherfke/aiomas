``aiomas.rpc``
==============

.. automodule:: aiomas.rpc
   :members:
   :exclude-members: Proxy

.. autoclass:: Proxy
   :members:
   :special-members:
